#!/usr/bin/env python

# -----------------------------------------------------------------------------
#   Copyright (C) 2020 University of Dundee. All rights reserved.

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# ------------------------------------------------------------------------------

# This script runs in parallel trackpy on the selected images.

import argparse
import getpass
import os
import sys
import tempfile
import time


# omero
from omero.gateway import BlitzGateway
from omero.model import PointI, PolylineI, RoiI
from omero.rtypes import rdouble, rint, rstring

# trackpy
import pandas as pd
import pims
import trackpy as tp

# cluster
from dask.distributed import Client, LocalCluster


tp.linking.Linker.MAX_SUB_NET_SIZE = 150
trackpy_version = "0.4.2"


def rgba_to_int(red, green, blue, alpha=255):
    return int.from_bytes([red, green, blue, alpha], byteorder='big',
                          signed=True)


def load_images(conn, id, data_type):
    """
    Loads the images
    """
    if data_type.lower() == "dataset":
        return list(conn.getObjects('Image', opts={'dataset': id}))
    elif data_type.lower() == "plate":
        images = []
        plate = conn.getObject("Plate", id)
        for well in plate.listChildren():
            index = well.countWellSample()
            for index in range(0, index):
                images.append(well.getImage(index))
        return images
    elif data_type.lower() == "image":
        return [conn.getObject("Image", id)]
    return None


def load_binary(image):
    """
    Load the binary data from OMERO and convert them into pims.Frame
    as expected by trackpy
    """
    pixels = image.getPrimaryPixels()
    size_z = image.getSizeZ()
    size_c = image.getSizeC()
    size_t = image.getSizeT()
    zct_list = []
    for t in range(size_t):
        for z in range(size_z):
            for c in range(size_c):
                zct_list.append((z, c, t))

    # Load all the planes as YX numpy array
    planes = pixels.getPlanes(zct_list)
    # convert the planes into frame for trackpy
    frames = []
    for i, p in enumerate(planes):
        f = pims.frame.Frame(p, frame_no=i)
        frames.append(f)
    return frames


def run_tracky(frames, image_id):
    particles = tp.batch(frames, diameter=11, minmass=4000, invert=True)
    # track filtering; values based on several bad wells encountered over years
    temp_particles = particles[(particles.signal > 950) & (particles['size'] < 2.5)]  # noqa
    if len(temp_particles) < 150:
        particles = particles[(particles.signal > 550) & (particles['size'] < 2.5)]  # noqa
    else:
        particles = temp_particles
    # particle linking with hot fix for sub_net_size problem in some wells
    try:
        linked_particles = tp.link_df(particles, search_range=15, memory=5)
    except Exception:
        # more stringent size filter
        particles = particles[(particles.signal > 950) & (particles['size'] < 2.3)]  # noqa
        linked_particles = tp.link_df(particles, search_range=15, memory=5)
    # track filtering based on 14 frames threshold
    filtered_tracks = tp.filter_stubs(linked_particles, threshold=14)
    return {image_id: filtered_tracks}


# Save results
def save_results(conn, results):
    for i, result in enumerate(results):
        print(result)
        for k, v in result.items():
            # reload the image
            image = conn.getObject("Image", k)
            data = save_tracks(conn, image, v)
            save_as_csv(conn, image, data)


def save_tracks(conn, image, tracks):
    """
    Save the tracks back to OMERO
    """
    columns = list(tracks.columns)
    columns.append("RoiID")
    columns.append("ImageID")
    general_df = pd.DataFrame(columns=columns)

    svc = conn.getUpdateService()
    count = tracks['particle'].nunique()
    for c in range(0, count):
        track = tracks.loc[tracks['particle'] == c]
        roi = RoiI()
        roi.setImage(image._obj)
        points = ""
        for index, row in track.iterrows():
            # if the radius changes, we should use ellipse
            # (point is for radius 5)
            point = PointI()
            point.x = rdouble(float(row['x']))
            point.y = rdouble(float(row['y']))
            point.theT = rint(int(row['frame']))
            roi.addShape(point)
            points += "%s,%s " % (str(row['x']), str(row['y']))
        polyline = PolylineI()
        polyline.points = rstring(points)
        polyline.fillColor = rint(rgba_to_int(255, 0, 255, 50))
        roi.addShape(polyline)
        if points:
            roi = svc.saveAndReturnObject(roi)
            # Set the ROI Id in the data frame
            track = track.copy()
            track['RoiID'] = roi.getId().getValue()
            general_df = general_df.copy().append(track, ignore_index=True)

    general_df['ImageID'] = image.getId()
    return general_df


def save_as_csv(conn, image, data):
    """
    Save data frame as CSV and link it to the image.
    """
    archive_name = "tracks_image_%s.csv" % image.getId()
    tmp = os.path.join(tempfile.gettempdir(), archive_name)
    data.to_csv(path_or_buf=tmp, index=False)
    namespace = "trackpy_version_%s" % trackpy_version
    file_ann = conn.createFileAnnfromLocalFile(tmp,
                                               mimetype="text/csv",
                                               ns=namespace, desc=None)
    image.linkAnnotation(file_ann)
    os.remove(tmp)


def prepare(client, images):
    # instead of loading the binary from omero, we could read from disk
    # passing the path to the image instead.
    futures = [client.submit(run_tracky, load_binary(i), i.getId()) for i in images]
    return futures


def gather_results(client, futures):
    return client.gather(futures)


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('id', help="The object id e.g. image's id, plate's id.")
    parser.add_argument('--type', default="plate", 
                        help="Load the images for the following type: image, dataset or plate")
    parser.add_argument('--username', default="trainer-1", help="The user to connect as")
    parser.add_argument('--server', default="outreach.openmicroscopy.org", 
                        help="The server to connect to")
    parser.add_argument('--port', default=4064)
    args = parser.parse_args(args)
    password = getpass.getpass()
    # Load the image corresponding to the file name
    # Create a connection
    try:
        conn = BlitzGateway(args.username, password, host=args.server,
                            port=args.port)
        conn.connect()
        conn.c.enableKeepAlive(60)
        images = load_images(conn, args.id, args.type)
        if images is None or len(images) == 0:
            print("no images to analyze")

         # Create-client
        cluster = LocalCluster()
        client = Client(cluster)

        futures = prepare(client, images)
        start = time.time()
        results = gather_results(client, futures)
        done = time.time()
        elapsed = (done - start) // 60
        print("Compute time (in minutes): %s" % elapsed)
        print("saving results...")
        save_results(conn, results)
    finally:
        conn.close()
        print("done")


if __name__ == "__main__":
    main(sys.argv[1:])